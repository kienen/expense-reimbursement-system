CREATE TABLE ers_user_roles (
	reimb_user_role_id INT PRIMARY KEY
	, user_role VARCHAR(20)
);
INSERT INTO ers_user_roles VALUES (1, 'EMPLOYEE');
INSERT INTO ers_user_roles VALUES (2, 'MANAGER');

CREATE TABLE ers_reimbursement_status (
	reimb_status_id INT PRIMARY KEY
	, reimb_status VARCHAR(20)
);

INSERT INTO ers_reimbursement_status VALUES (1, 'PENDING');
INSERT INTO ers_reimbursement_status VALUES (2, 'APPROVED');
INSERT INTO ers_reimbursement_status VALUES (3, 'DENIED');

CREATE TABLE ers_reimbursement_type (
	reimb_type_id INT PRIMARY KEY
	, reimb_type VARCHAR(20)
);
INSERT INTO ers_reimbursement_type VALUES (1, 'LODGING');
INSERT INTO ers_reimbursement_type VALUES (2, 'TRAVEL');
INSERT INTO ers_reimbursement_type VALUES (3, 'FOOD');
INSERT INTO ers_reimbursement_type VALUES (4, 'OTHER');

CREATE TABLE ers_users (
	ers_users_id SERIAL PRIMARY KEY
	, ers_username VARCHAR(50) UNIQUE NOT NULL
	, ers_password VARCHAR(100) NOT NULL
	, user_first_name VARCHAR(100) NOT NULL
	, user_last_name VARCHAR(100) NOT NULL
	, user_email VARCHAR(150) UNIQUE NOT NULL
	, user_role_id INT NOT NULL
	, FOREIGN KEY (user_role_id) REFERENCES ers_user_roles(reimb_user_role_id)
);

INSERT INTO ers_users VALUES (5, 'manager', 'pass', 'trevin', 'c', 'manager@revature.net', 2);
INSERT INTO ers_users VALUES (2, 'santi','pass','santi', 'ramirez', 'santi@revature.net', 1);
INSERT INTO ers_users VALUES (3, 'william','pass', 'william', 'sculley', 'will@revature.net', 1);
INSERT INTO ers_users VALUES (4, 'lj','pass', 'jae', 'lee', 'lj@revature.net', 1);

CREATE TABLE ers_reimbursement (
	reimb_id SERIAL PRIMARY KEY
	, reimb_amount NUMERIC NOT NULL
	, reimb_submitted timestamptz NOT NULL
	, reimb_descrip VARCHAR (250) NULL
	, reimb_receipt bytea NULL
	, reimb_author INT NULL
	, FOREIGN KEY (reimb_author) REFERENCES ers_users(ers_users_id)
	, reimb_resolved timestamptz  NULL
	, reimb_resolver INT NULL
	, FOREIGN KEY (reimb_author) REFERENCES ers_users(ers_users_id)
	, reimb_status_id INT NULL
	, FOREIGN KEY (reimb_status_id) REFERENCES ers_reimbursement_status(reimb_status_id)
	, reimb_type_id INT NULL
	, FOREIGN KEY (reimb_type_id) REFERENCES ers_reimbursement_type (reimb_type_id)
);


--ALTER TABLE ers_reimbursement ALTER COLUMN reimb_resolved SET DATA TYPE timestamptz;

--ALTER TABLE ers_reimbursement ALTER COLUMN reimb_resolved drop NOT NULL;
--ALTER TABLE ers_reimbursement ALTER COLUMN reimb_amount SET DATA TYPE NUMERIC ;
--alter table ers_users alter column ers_password TYPE VARCHAR (100);
--CREATE SEQUENCE ers_users_ers_users_id_seq OWNED BY ers_users.ers_users_id;
--SELECT setval('ers_users_ers_users_id_seq', coalesce(max(ers_users_id), 0) + 1, false) FROM ers_users;
--ALTER TABLE ers_users ALTER COLUMN ers_users_id SET DEFAULT nextval('ers_users_ers_users_id_seq'); 


