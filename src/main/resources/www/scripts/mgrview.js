window.onload = function () {
  getTix();
  getUser();
};

function fetchTrigger(event) {
  document.getElementById("history").innerText = "A bunch of stuff";
}

async function getTix() {
  const responsePayload = await fetch(`http://localhost:9001/mgr/pendingTix`);
  const tickets = await responsePayload.json();

  //console.log(typeof tickets);
  //let tickets = JSON.parse(JSONtickets);
  let box = document.getElementById("ticket-container");

  console.log(tickets);
  //console.log(tickets[0]);
  for (let i = 0; i < tickets.length; i++) {
    let htmlTicket = document.createElement("div");
    htmlTicket.className = "item";
    box.appendChild(htmlTicket);
    let list = document.createElement("ul");
    htmlTicket.appendChild(list);

    let submitted = new Date(tickets[i].submitted);
    let options = {dateStyle: 'short', timeStyle: 'short'};

    addListItem(list, "Submitter: " + tickets[i].authorName);
    addListItem(list, "Amount: $" + tickets[i].amount.toFixed(2));
    addListItem(list, "Description: " + tickets[i].description);
    addListItem(list, "Submitted: " + submitted.toLocaleString("en-US", options));
    //addListItem(list, "Status " + tickets[i].status);
    addListItem(list, "Type " + tickets[i].type);

    let aButton = document.createElement("button");
    aButton.setAttribute("onclick", `approve(${tickets[i].id})`);
    aButton.setAttribute("id", `A${tickets[i].id}`)
    aButton.innerText = "Approve";

    let bButton = document.createElement("button");
    bButton.setAttribute("onclick", `deny(${tickets[i].id})`);
    bButton.setAttribute("id", `D${tickets[i].id}`)
    bButton.innerText = "Deny";

    htmlTicket.appendChild(aButton);
    htmlTicket.appendChild(bButton);
  }
}

function addListItem(list, text) {
  let item = document.createElement("li");
  item.innerText = text;
  list.appendChild(item);
}

async function approve(id) {
  console.log(id);
  let body = fetch(`http://localhost:9001/mgr/approve`, {
    method: "post",
    headers: {
      "content-type": "application/json",
    },
    body: JSON.stringify({ id: id }),
  })
    
    .then(function (daResponse) {
      console.log(daResponse);
      const convertedResponse = daResponse
      console.log(convertedResponse);
      if (daResponse.ok ) {
        let dButton = document.getElementById(`D${id}`);
        let aButton = document.getElementById(`A${id}`);
        dButton.hidden = true;
        aButton.hidden = true;
		
		msg = document.createTextNode("Approved");
        dButton.parentElement.appendChild(msg);
      }
      

    })
    .catch((stuff) => {
      console.log("Error" + stuff);
    });
}

async function deny(id) {
  console.log(id);
  let body = fetch(`http://localhost:9001/mgr/deny`, {
    method: "post",
    headers: {
      "content-type": "application/json",
    },
    body: JSON.stringify({ id: id }),
  })
    //TODO confirmation

    .then(function (daResponse) {
      console.log(daResponse);
      const convertedResponse = daResponse
      console.log(convertedResponse);
      if (daResponse.ok ) {
        let dButton = document.getElementById(`D${id}`);
        let aButton = document.getElementById(`A${id}`);
        dButton.hidden = true;
        aButton.hidden = true;
		
		msg = document.createTextNode("Denied");
        dButton.parentElement.appendChild(msg);
      }
    })
    .catch((stuff) => {
      console.log("Error" + stuff);
    });
}


async function getUser() {
  const responsePayload = await fetch(`http://localhost:9001/secure/getUser`);
  const user = await responsePayload.json();

  document.getElementById("userHeader").innerText =
    "Greetings, " + user.firstName;
}
/*
  function notes() {
    // Just here to remember it
    //Dynamically create form
    <form action="#">
    <input type="text"/>
    <input type="button" onclick="yourJsFunction();"/>
</form>
  }
  
  */
