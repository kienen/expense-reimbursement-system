window.onload = function () {
  getTix();
  getUser();
};

function fetchTrigger(event) {
  document.getElementById("history").innerText = "A bunch of stuff";
}

async function getTix() {
  const responsePayload = await fetch(`http://localhost:9001/secure/userTix`);
  const tickets = await responsePayload.json();

  //console.log(typeof tickets);
  //let tickets = JSON.parse(JSONtickets);
  let box = document.getElementById("ticket-container");

  console.log(tickets);
  //console.log(tickets[0]);
  for (let i = 0; i < tickets.length; i++) {
    let htmlTicket = document.createElement("div");
    htmlTicket.className = "item";
    box.appendChild(htmlTicket);
    let list = document.createElement("ul");
    htmlTicket.appendChild(list);

	let submitted = new Date(tickets[i].submitted);
	let resolved = new Date(tickets[i].resolved);
    let options = {dateStyle: 'short', timeStyle: 'short'};

    addListItem(list, "Amount: $" + tickets[i].amount.toFixed(2));
    addListItem(list, "Description: " + tickets[i].description);
    addListItem(list, "Submitted: " + submitted.toLocaleString("en-US", options));
    addListItem(list, "Status: " + tickets[i].status);
    addListItem(list, "Type " + tickets[i].type);
    if (tickets[i].status != "PENDING") {
        addListItem(list, "Resolved by: " + tickets[i].resName);  
        addListItem(list, "Resolved on: " + resolved.toLocaleString("en-US", options));
    }
  }
}
function addListItem(list, text) {
  let item = document.createElement("li");
  item.innerText = text;
  list.appendChild(item);
}

async function getUser() {
  const responsePayload = await fetch(`http://localhost:9001/secure/getUser`);
  const user = await responsePayload.json();

  document.getElementById("userHeader").innerText =
    "Greetings, " + user.firstName;
}
