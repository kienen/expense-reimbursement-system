/**
 * 
 */
package fastMoney;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import Models.Reimb;
import Models.Status;
import Models.User;


/**
 * @author Kienen
 *
 */
public class Service {
	final static Logger logdao = Logger.getLogger(Service.class);
	private DAOLayer Database;

	
	/**
	 * 
	 */
	public Service() {
		super();
		this.Database = new DAOLayer();
	}
	
	public Service(DAOLayer Database) {
		super();
		this.Database = Database;
	}

	/**
	 * User login
	 * @param username
	 * @param password
	 * @return
	 */
	public User login(String username, String password) {

		User dbUser = Database.findUser(username);
		if (dbUser != null && dbUser.checkPassword(password)) {
			return dbUser;
		}
		System.out.println("Invalid username or password");
		return null;

	}
	
	/**
	 * 
	 * @param ticket
	 * @return
	 */
	public boolean createTicket (Reimb ticket) {
		return Database.add(ticket);
	}
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	public boolean createUser(User user) {
		boolean success = Database.add(user);
		if (success) {
			String msg = "Welcome!\n Visit http://localhost:9001 to login \n Your temporary password is CorrectHorseBatteryStaple.";
			Email.sendMail(msg, user.getEmail());
		}
		
		return success;
		
	}

	/**
	 * 
	 * @param user
	 * @return
	 */
	public Reimb[] viewTicketsByUser(User user) {
		logdao.debug(user);
		return Database.getRequests(user);
	}

	/**
	 * 
	 * @param ticket
	 * @param status
	 * @param mgr
	 * @return
	 */
	public boolean updateStatus(int ticket, Status status, int mgr) {
		return Database.updateStatus(ticket, status, mgr);
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public  Reimb[] viewTicketsByStatus(Status status) {
		logdao.debug("view by status");
		return Database.getRequests(status);
	}
	
	/**
	 * 
	 * @param userID
	 * @return
	 */
	public  String findEmployeeName (int userID) {
		return Database.findEmployeeName(userID);
	}
	
	/**
	 * 
	 * @param user
	 * @param oldPass
	 * @param newPass
	 * @return
	 */
	public boolean updatePassword (User user, String oldPass, String newPass) {
		return Database.updatePassword(user.getId(), oldPass, newPass);
	}

	/**
	 * Main for testing
	 * @param args
	 */
//	public static void main(String[] args) {
//		
//		//DAOService DAO = new DAOService ();
//
//		logdao.setLevel(Level.ALL);
//		logdao.trace("DAOService main");
//		User user = login("trevin", "pass");
//		System.out.println(user);
//	}

}
