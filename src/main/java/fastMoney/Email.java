package fastMoney;

import java.time.Instant;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.sun.mail.smtp.SMTPAddressSucceededException;

import javax.activation.*;

public class Email {
	
	final static Logger logem = Logger.getLogger(Email.class);
	
	
	/**
	 * Send the string to the supplied email.
	 * @param msg
	 * @param email
	 */
	public static void sendMail (String msg , String email) {
		Properties prop = new Properties();
		prop.setProperty("mail.debug", "true");
		prop.setProperty("mail.smtp.auth", "true");
		prop.setProperty("mail.smtp.starttls.enable", "true");
		prop.setProperty("mail.smtp.ssl.protocols", "TLSv1.2");
		prop.setProperty("mail.smtp.host", "chi106.greengeeks.net");
		prop.setProperty("mail.smtp.port", "465");
		prop.setProperty("mail.smtp.ssl.enable", "true");
		prop.setProperty("mail.smtp.ssl.trust", "chi106.greengeeks.net");
		Session session = Session.getInstance(prop, new Authenticator() {
		    @Override
		    protected PasswordAuthentication getPasswordAuthentication() {
		    	//username and password stored in a separate file outside of git
		        return new PasswordAuthentication(LoginInfo.username, LoginInfo.password);
		    }
		    
		});
		//session.setDebug(true);
		
		try  {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(LoginInfo.username));
			message.setRecipients(
			  Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject(Instant.now().toString());
			logem.warn("SENDING TO DUMMY EMAIL WITH DUMMY MESSAGE");
			

			

			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setContent(msg, "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);

			message.setContent(multipart);

			Transport.send(message);
		} catch (SendFailedException err) {
			err.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} 
	}
	
	public String buildMail (String...email) {
		//TODO
		String newLine = System.getProperty("line.separator");
		return newLine;
	}
	
// Main for testing	
//	public static void main(String[] args) {
//		logem.setLevel(Level.ALL);
//		logem.trace("Email main");
//		String msg = "This is my first email using JavaMailer";
//		//sendMail(msg);
//		logem.trace("end");
//	}
}
