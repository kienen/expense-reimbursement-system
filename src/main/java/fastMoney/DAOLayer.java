package fastMoney;


import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.postgresql.util.PSQLException;

import Models.Reimb;
import Models.Role;
import Models.Status;
import Models.Type;
import Models.User;

import java.time.*;
import java.util.UUID;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class DAOLayer {
	private String url = "jdbc:postgresql://34.102.69.108/Bursar_test";
	private String username = "postgres";
	private String password = "p4ssw0rd";
	
	final static Logger logjdbc = Logger.getLogger(DAOLayer.class);
	private Map<Integer, String> nameCache = null;

	
	/**
	 * @param url
	 * @param username
	 * @param password
	 */
	public DAOLayer(String url, String username, String password) {
		if (url != null) {
			this.url = url;
		}
		if (username != null) {
			this.username = username;
		}
		if (password != null) {
			this.password = password;
		}
	}

	/**
	 * 
	 */
	public DAOLayer() {
		super();
	}
	/**
	 * Main method for help with debugging. 
	 * @param args
	 */
	public static void main(String[] args) {
		logjdbc.setLevel(Level.ALL);
		logjdbc.trace("JDBC main");
		DAOLayer Database = new DAOLayer();
		
		//User user = Database.findUser("santi");
		//System.out.println(Database.findUser("trevin"));
		//Database.add(new Reimb(2, Instant.now(), "desc", 3, Type.LODGING));
		//Reimb [] myTickets = Database.getRequests(user);
		//Database.updateStatus(3, Status.APPROVED, 1);
		//Database.findEmployeebyID(1);
//		for (int x = 1; x <5 ; x++ ) {
//			Database.updatePassword(x, "pass", User.encrypt("pass"));
//		}
//		System.out.println(User.encrypt("pass").length());
		Database.getRequests(null, Status.PENDING);
		
	}
	
	/**
	 * Return the user with the argument string passed.
	 * @param accountName
	 * @return
	 */
	public User findUser(String accountName) {
		logjdbc.trace("JDBC findUser");
		logjdbc.setLevel(Level.ALL);
		User thisUser = null;

		try (Connection conn = DriverManager.getConnection(url, username, password)) {

			String ps = "SELECT * FROM ers_users WHERE ers_username = ? OR user_email = ?";

			PreparedStatement statement = conn.prepareStatement(ps);
			statement.setString(1, accountName);
			statement.setString(2, accountName);
			ResultSet results = statement.executeQuery();
			if (results.next()) {
				logjdbc.debug("ers_users_id " + results.getInt(1));
				logjdbc.debug("ers_username " + results.getString(2));
				logjdbc.debug("ers_password " + results.getString(3));
				logjdbc.debug("user_role_id " + results.getInt("user_role_id"));

				//User.Role role = User.Role.fromInteger(results.getInt("user_role_id")); 
				thisUser = new User(results.getInt("ers_users_id"),
									results.getString("ers_username"),
									results.getString("ers_password"),
									results.getString("user_first_name"),
									results.getString("user_last_name"),
									results.getString("user_email"),
									Role.fromInt(results.getInt("user_role_id"))
									);
			}
		} catch (SQLException e) {
			logjdbc.error("find account ", e);
		}

		return thisUser;
	}
	
	public boolean add(Reimb ticket) {
		logjdbc.trace("JDBC add");
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			String sqlStatement = "INSERT INTO ers_reimbursement (reimb_amount, " //1
																	+ "reimb_submitted, " //2
																	+ "reimb_descrip, " //3
																	+ "reimb_author, " //4
																	+ "reimb_status_id, " //5
																	+ "reimb_type_id) " //6
																	+ "VALUES(?,?,?,?,?,?)";

			PreparedStatement ps = conn.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, ticket.getAmount());
			ps.setObject(2, ticket.getSubmitted());
			ps.setString(3, ticket.getDescription());
			ps.setInt(4, ticket.getAuthor());
			ps.setInt(5, 1);
			ps.setInt(6, ticket.getType().toInt());
			
			ps.executeUpdate();
			ResultSet generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				// get primary key from database and store in local object
				ticket.setId(generatedKeys.getInt(1));
				ticket.setStatus(Status.PENDING);
				logjdbc.debug("ticket id created:" + ticket.getId());
			} else {
				throw new PSQLException("Creating ticket failed, no ID obtained.", null);
			}

			return true;

		} catch (PSQLException err) {
			logjdbc.error("add ticket", err);
			System.out.println("SQL Error adding ticket");
			return false;
		} catch (SQLException err) {
			logjdbc.error("Add Account error", err);
			
			return false;
		}
	}
	
	/** 
	 * Create a user in the database.
	 * @param user
	 * @return
	 */
	public boolean add(User user) {
		logjdbc.trace("JDBC add user");
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			String sqlStatement = "INSERT INTO ers_user (ers_username, " //1
																	+ "user_role_id, " 
																	+ "user_first_name, " //2
																	+ "user_last_name, " //3
																	+ "user_email, " //4
																	+ "ers_password) " 
																	+ "VALUES(?,1,?,?,?,?)";

			PreparedStatement ps = conn.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, user.getUsername());
			ps.setInt(2, 1);
			ps.setString(3, user.getFirstName());
			ps.setString(4, user.getLastName());
			ps.setString(5, user.getEmail());
			ps.setString(6, "CorrectHorseBatteryStaple");
			
			logjdbc.debug(ps);
			ps.executeUpdate();
			ResultSet generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				// get primary key from database and store in local object
				user.setId(generatedKeys.getInt("id"));
				
				logjdbc.debug("user created:" + user.getUsername());
			} else {
				throw new PSQLException("Creating user failed, no ID obtained.", null);
			}

			return true;

		} catch (PSQLException err) {
			logjdbc.error("add ticket", err);
			System.out.println("SQL Error adding ticket");
			return false;
		} catch (SQLException err) {
			logjdbc.error("Add Account error", err);
			
			return false;
		}
	}

	
	/**
	 * Return all requests.
	 * If either argument is null it is ignored.
	 * If both arguments are ignored then it will return all requests.
	 * @param user
	 * @param status
	 * @return
	 */
	public Reimb [] getRequests(User user, Status status) {
		logjdbc.trace("getRequests");
		
		Reimb request;
		List <Reimb> requests = new ArrayList<> ();
		
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			String sqlCommand = "SELECT r.* \n" +
					", concat( u2.user_first_name, ' ', u2.user_last_name ) AS \"resolver_name\"  \n" + 
					", concat( u.user_first_name, ' ', u.user_last_name) AS \"auth_name\"  \n" +
					"FROM ers_reimbursement r \n" +
					"LEFT JOIN ers_users u ON r.reimb_author = u.ers_users_id \n" +
					"LEFT JOIN ers_users u2 ON r.reimb_resolver = u2.ers_users_id \n";
			
			if (user != null) {
				sqlCommand += " WHERE r.reimb_author = " + user.getId() + ";";
			} else if (status != null) {
				sqlCommand += " WHERE r.reimb_status_id = " + status.toInt() + ";";
			}
			
			logjdbc.trace(sqlCommand);
			PreparedStatement statement = conn.prepareStatement(sqlCommand);

			ResultSet results = statement.executeQuery();
			while (results.next()) {
				logjdbc.debug("Downloading Reimbursement");
//				logjdbc.debug(results.getInt("reimb_id"));
//				logjdbc.debug(results.getDouble("reimb_amount"));
//				logjdbc.debug(results.getString("reimb_author"));
//				logjdbc.debug(results.getString("reimb_descrip"));
//				logjdbc.debug(results.getObject("reimb_submitted"));
//				logjdbc.debug(results.getTimestamp("reimb_submitted").toInstant());
				
				request = new Reimb(results.getInt("reimb_id"), 							//int id
									results.getDouble("reimb_amount"), 						//double amount, 
									results.getInt("reimb_author"), 						//int author,
									(results.getTimestamp("reimb_submitted")), 	//LocalDateTime submitted, 
									results.getString("reimb_descrip"), 					//String description, 
									results.getBlob("reimb_receipt"),  						//Blob receipt
									Type.fromInt(results.getInt("reimb_type_id")), 			//Type type))
									Status.fromInt(results.getInt("reimb_status_id")) 		//Status status
									);
				
				request.setAuthorName(results.getString("auth_name"));
				if (request.getStatus() != Status.PENDING) {
					request.setResolved((results.getTimestamp("reimb_resolved")));
					request.setResolver(results.getInt("reimb_resolver"));
					request.setResName(results.getString("resolver_name"));
					
				}
				
				requests.add(request);
			}

			// return inspectors.toArray(new Inspector[inspectors.size()]);
			return requests.toArray(new Reimb[0]);
		} catch (SQLException e) {
			logjdbc.error("getRequest", e);
			logjdbc.error("DB Error finding requests");
			return null;
		}
		
	}
	
	
	/**
	 * Sugar syntax
	 * @param user
	 * @return
	 */
	public Reimb [] getRequests(User user) {
		return getRequests(user, null);
	}
	
	/**
	 * Sugar syntax
	 * @param status
	 * @return
	 */
	public Reimb [] getRequests(Status status) {
		logjdbc.debug(status);
		return getRequests(null, status);
	}
	
	
	/**
	 * Change the status of a request
	 * @param ticketID
	 * @param newStatus
	 * @param managerID
	 * @return
	 */
	public boolean updateStatus (int ticketID, Status newStatus, int managerID) {
		logjdbc.trace("Update Status");
		logjdbc.trace("Id: " + ticketID);
		logjdbc.trace("stat: " + newStatus);
		logjdbc.trace("mgr: " + managerID);
		int affectedRows = 0;
		try (Connection conn = DriverManager.getConnection(url, username, password)) {

			String sqlCommand = "UPDATE ers_reimbursement SET reimb_status_id = ?, "
														    + "reimb_resolved = ?, "
														    + "reimb_resolver = ? "
														    + "WHERE reimb_id = ?;";

			PreparedStatement statement = conn.prepareStatement(sqlCommand);
			statement.setInt(1, newStatus.toInt());
			statement.setObject(2, Timestamp.from(Instant.now()));
			statement.setInt(3, managerID);
			statement.setInt(4, ticketID);  
			
			affectedRows = statement.executeUpdate();
			
			if (affectedRows == 1 ) {
				logjdbc.debug("Updated " + ticketID);
				return true;
			}
			

		} catch (SQLException e) {
			logjdbc.error("updateStatus", e);
		}

		logjdbc.error("affectedRows: " + affectedRows);
		return false;
	}
	
	
	/**
	 * Change a user password
	 * @param userID
	 * @param oldPass
	 * @param newPass
	 * @return
	 */
	public boolean updatePassword (int userID, String oldPass, String newPass) {
		int affectedRows = 0;
		try (Connection conn = DriverManager.getConnection(url, username, password)) {

			
			String sqlCommand = "UPDATE ers_users SET ers_password = ? WHERE ers_users_id = ? AND ers_password = ?;";

			PreparedStatement statement = conn.prepareStatement(sqlCommand);
			statement.setString(1, newPass);
			statement.setInt(2, userID);
			statement.setString(3, oldPass);
			
			affectedRows = statement.executeUpdate();
			
			if (affectedRows == 1 ) {
				logjdbc.debug("Updated " + userID);
				return true;
			}
			

		} catch (SQLException e) {
			logjdbc.error("updateStatus", e);
		}

		logjdbc.error("affectedRows: " + affectedRows);
		return false;
	}	
	

	/** 
	 * caches a map of Employees and their primary keys
	 *  returns the full name of the user with the given primary key
	 * @param ID
	 * @return
	 */
	public String findEmployeeName(int ID) {
		logjdbc.trace("Find Employee");
		if (nameCache != null && nameCache.containsKey(ID)) {
			logjdbc.trace("Found in cache: " + nameCache.get(ID));
			return nameCache.get(ID);
		}
		nameCache = new HashMap<Integer, String>();
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			String ps = "SELECT ers_users_id, concat(user_first_name, ' ' , user_last_name) AS \"Name\" FROM ers_users;";

			PreparedStatement statement = conn.prepareStatement(ps);

			ResultSet results = statement.executeQuery();
			while (results.next()) {
				logjdbc.debug("Caching User");
				logjdbc.debug(results.getInt("ers_users_id"));
				logjdbc.debug(results.getString("Name"));
				nameCache.put(results.getInt("ers_users_id"), results.getString("Name"));
			}

			
		} catch (SQLException e) {
			logjdbc.error("findID", e);
			logjdbc.warn("DB Error finding employees");
		}
		if (nameCache.containsKey(ID)) {
			return nameCache.get(ID);
		}

		logjdbc.debug("Employee Not Found");
		return null;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	
}

