package Models;

public enum Status {
	PENDING, APPROVED, DENIED;
	
	public static Status fromInt(int x) {
		switch (x) {
		case 1:
			return PENDING;
		case 2:
			return APPROVED;
		case 3:
			return DENIED;
		}

		return null;
	}
	
	public int toInt() {
		switch (this) {
		case PENDING:
			return 1;
		case APPROVED:
			return 2;
		case DENIED:
			return 3;
		}
		
		return 0;
	}
}
