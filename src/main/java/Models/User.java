/**
 * 
 */
package Models;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * @author Kienen
 *
 */
public class User {
	final static Logger loguser = Logger.getLogger(User.class);
	
	private int id;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private Role role;

	private final static String tempPass = "d74ff0ee8da3b9806b18c877dbf29bbde50b5bd8e4dad7a3a725000feb82e8f1";

	/**
	 * @param id
	 * @param username
	 * @param password
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param role_id
	 */
	public User(int id, String username, String password, String firstName, String lastName, String email,
			Role role_id) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.role = role_id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		loguser.setLevel(Level.ALL);
		loguser.trace("user main");

	}
	
	

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	public String toString() {
		return firstName + " " + lastName;
	}

	public boolean checkPassword (String str) {
		String cryptostr = encrypt (str);
		//loguser.debug("checkPassword" + password + " " + cryptostr);
		//loguser.debug(password);
		if (password.equals(cryptostr)) {
			return true;
		}
		return false;

	}
	
	public static String encrypt(String passwordToHash) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			// md.update(salt);
			byte[] bytes = md.digest(passwordToHash.getBytes());
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			loguser.debug("encrypted password " + sb.toString());
			return sb.toString();

		} catch (NoSuchAlgorithmException e) {
			System.out.println("Encryption disabled.");
			return passwordToHash;
		}

	}
}
