/**
 * 
 */
package Models;

import java.sql.Blob;
import java.sql.Timestamp;
//import java.time.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;


/**
 * @author Kienen
 *
 */
public class Reimb {
	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}



	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}



	private int id;
	private double amount;
	private int author;
	private String authorName;
	private Timestamp submitted;
	private String description;
	private Blob receipt;
	private Type type;
	
	private int resolver;
	private String resName;
	private Timestamp  resolved;
	private Status status;
	
	final static Logger logre = Logger.getLogger(Reimb.class);
	
	
	
	/**
	 * @param amount
	 * @param submitted
	 * @param resolved
	 * @param description
	 * @param author
	 * @param type
	 * 
	 * Constructor to upload to database
	 */
	public Reimb(double amount, Timestamp  submitted, String description, int author,
			Type type) {
		this.amount = amount;
		this.submitted = submitted;
		this.description = description;
		this.author = author;
		this.type = type;
	}



	/**
	 * @param id
	 * @param amount
	 * @param author
	 * @param submitted
	 * @param description
	 * @param receipt
	 * @param type
	 * @param status
	 * 
	 * Constructor to download from database
	 */
	public Reimb(int id, double amount, int author, Timestamp submitted, String description, Blob receipt,
			Type type, Status status) {
		this.id = id;
		this.amount = amount;
		this.author = author;
		this.submitted = submitted;
		this.description = description;
		this.receipt = receipt;
		this.type = type;
		//this.resolver = resolver;
		//this.resolved = resolved;
		this.status = status;
	}



	/**
	 * Default constructor
	 */
	public Reimb() {
		super();
	}



	/**
	 * @return the resName
	 */
	public String getResName() {
		return resName;
	}



	/**
	 * @param resName the resName to set
	 */
	public void setResName(String resName) {
		this.resName = resName;
	}



	/**
	 * @return the receipt
	 */
	public Blob getReceipt() {
		return receipt;
	}



	/**
	 * @return the authorName
	 */
	public String getAuthorName() {
		return authorName;
	}



	/**
	 * @param authorName the authorName to set
	 */
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}



	/**
	 * @param receipt the receipt to set
	 */
	public void setReceipt(Blob receipt) {
		this.receipt = receipt;
	}



	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}



	/**
	 * @return the submitted
	 */
	public Timestamp  getSubmitted() {
		return submitted;
	}



	/**
	 * @param submitted the submitted to set
	 */
	public void setSubmitted(Timestamp  submitted) {
		this.submitted = submitted;
	}



	/**
	 * @return the resolved
	 */
	public Timestamp  getResolved() {
		return resolved;
	}



	/**
	 * @param resolved the resolved to set
	 */
	public void setResolved(Timestamp resolved) {
		this.resolved = resolved;
	}



	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}



	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}



	/**
	 * @return the author
	 */
	public int getAuthor() {
		return author;
	}



	/**
	 * @param author the author to set
	 */
	public void setAuthor(int author) {
		this.author = author;
	}



	/**
	 * @return the resolver
	 */
	public int getResolver() {
		return resolver;
	}



	/**
	 * @param resolver the resolver to set
	 */
	public void setResolver(int resolver) {
		this.resolver = resolver;
	}



	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}



	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}



	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}



	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	public String toString () {
		return "Ticket: " + authorName + " " + id;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		logre.setLevel(Level.ALL);
		logre.trace("reimb main");
	}

}


