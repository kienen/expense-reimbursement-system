/**
 * 
 */
package Models;

/**
 * @author Kienen
 *
 */
public enum Type {
	LODGING, TRAVEL, FOOD, OTHER;
	
	public static Type fromInt(int x) {
		switch (x) {
		case 1:
			return LODGING;
		case 2:
			return TRAVEL;
		case 3:
			return FOOD;
		case 4:
			return OTHER;
		}

		return null;
	}
	
	public int toInt() {
		switch (this) {
		case LODGING:
			return 1;
		case TRAVEL:
			return 2;
		case FOOD:
			return 3;
		case OTHER:
			return 4;
		}
		
		return 0;
	}
}
