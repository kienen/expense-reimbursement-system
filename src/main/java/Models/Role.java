/**
 * 
 */
package Models;

/**
 * @author Kienen
 *
 */
public enum Role {
	EMPLOYEE, MANAGER;

	public static Role fromInt(int x) {
		switch (x) {
		case 1:
			return EMPLOYEE;
		case 2:
			return MANAGER;
		}

		return null;
	}
	
	public int toInt () {
		switch (this) {
		case EMPLOYEE:
			return 1;
		case MANAGER:
			return 2;
		}
		
		return 0;
	}
}
