package web;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;

import Models.Role;
import Models.User;
import fastMoney.Service;
import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import io.javalin.http.Context;
import io.javalin.http.ForbiddenResponse;

public class Server {
	private static Dispatcher dispatcher;
	final static Logger logServer = Logger.getLogger(Server.class);
	
	///////////////MIDDLEWARE LOGIC
	public static void secure(Context ctx) {
		logServer.trace("employee secure");
		try {
			User user = ctx.sessionAttribute("currentUser");
			if (user == null) {
				ctx.redirect("/");
				throw new ForbiddenResponse("Off limits!"); 
				
			}
		} catch (Exception err){
			ctx.redirect("/");
			throw new ForbiddenResponse("Off limits!"); 
		}
		

		
	}
	
	public static void mgr(Context ctx) {
		logServer.trace("manager secure");
		try {
			User user = ctx.sessionAttribute("currentUser");
			if (user == null || user.getRole() != Role.MANAGER) {
				ctx.redirect("/");
				throw new ForbiddenResponse("Off limits!"); 
			}
		} catch (Exception err){
			ctx.redirect("/");
			throw new ForbiddenResponse("Off limits!"); 
		}

		
	}

	public static void main(String[] args) {
		logServer.setLevel(Level.ALL);
		logServer.trace("Server main");
		
		Javalin server = Javalin.create(
				config -> {
					config.addStaticFiles(
							staticFiles -> {
								staticFiles.directory="www";
								staticFiles.hostedPath="/";
								staticFiles.location = Location.CLASSPATH;		
							});
				}).start(9001);
		
		//TODO 
		server.before("/secure/*", Server::secure);
		server.before("/mgr/*", Server::mgr);
		
		dispatcher = new Dispatcher (server);
		
		
		/////////////////////////////////////////////////////////////
		//Trevin
/*
		server.get("/hello", (stuff) -> {
			stuff.result("Hello World");
		});
		
		server.post("/hello", (stuff) -> {
			stuff.result("Hola Me Llamo Trevin").status(418);
		});
		
		////////////////////HOW DO I REDIRECT TO A DIFFERENT HTML PAGE?////////////////////
		server.get("/api/redirect-demo", context ->{
		System.out.println("In redirect demo");
		
		//you could, say, check their login credentials here THEN send them to the
		//	home page
		
		//THIS is how you programmatically redirect someone to a different html page
		context.redirect("/secondPage.html");
		});

		///JSON RESPONSE////
		server.get("/api/object-getter", (context) -> {
			context.contentType("application/json");
			String disgusting = "{ \"field1\": 5, \"field2\": [5,10,\"hello\"]}";
			System.out.println(disgusting);
			context.result( disgusting);
		});
		
		server.get("/api/json-getter", (context) -> {
			context.json(new example.SuperHumans("Danny Boy", "Electromagnetism", 250_000));
		});
		
		server.get("/api/apple", (context)->{
			context.json(new example.SuperHumans("Apple Jupiter", "Flesh Fruit", 120_000));
		});
		
		server.get("/api/pepper", (context)->{
			context.json(new example.SuperHumans("Pepper Jupiter", "Drowsy Fist", 120_000));
		});
		
		//Query Params
		server.get("/api/queryparam-demo/", context -> {
			String firstParam = context.queryParam("appleJacks");
			String secondParam = context.queryParam("fruitLoops");
			
			System.out.println(firstParam);
			System.out.println(secondParam);
			
			context.result("This: "+ firstParam + " " + secondParam);
		});
		
		server.get("/api/{doritos}/pathparam-demo/{pringles}", context -> {
			String firstParam = context.pathParam("pringles");
			String secondParam = context.pathParam("doritos");
			
			System.out.println(firstParam);
			System.out.println(secondParam);
			
			context.result("This: "+ firstParam + " " + secondParam);
		});
		
		server.post("/api/form-field-demo", context -> {
			String firstParam = context.formParam("pringles");
			String secondParam = context.formParam("doritos");
			
			System.out.println(firstParam);
			System.out.println(secondParam);
			
			context.result("This: "+ firstParam + " " + secondParam);
		});
		
		server.post("/api/requestbody-demo", context->{
			System.out.println("This: " +context.body());
			String body = context.body();
			//example.SuperHumans
			
			context.result("This: " + body);
		});

	*/
	}

}
