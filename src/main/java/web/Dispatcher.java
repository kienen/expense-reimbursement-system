/**
 * 
 */
package web;

import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*; //this is where "path" came from

import org.apache.log4j.Logger;

import fastMoney.Service;

/**
 * @author Kienen
 *
 */
public class Dispatcher {
	// Javalin server;
	Dispatcher dispatcher;
	final static Logger logDis = Logger.getLogger(Dispatcher.class);

	///////////// CONSTRUCTOR
	public Dispatcher(Javalin server) {
		// TODO create endpoints
		// setupAllPaths(server);

		loginPaths(server);
		apiPaths(server);
		mgrPaths(server);
		/*
		 * server.routes(() -> { path("/login", () -> { logDis.trace("login");
		 * post(Controller::login);// endpoint: /api/user/login });
		 * 
		 * path("/secure/add", () -> { logDis.trace("add"); post(Controller::add);
		 * //get(Controller::getallrequests); });
		 * 
		 * path("/secure/viewUserTickets",() -> { logDis.trace("viewUserTickets");
		 * post(Controller::viewUserTickets); });
		 * 
		 * 
		 * });
		 */
	}

	/*
	  Prototype 
	  path("/secure/", () -> { 
	  logDis.trace(""); 
	  post(Controller::); 
	  });
	 */
	public static void loginPaths(Javalin server) {
		server.routes(() -> {
			path("/login", () -> {
				post(Controller::login);
			});
			


		});
	}

	public static void apiPaths(Javalin server) {
		server.routes(() -> {
			  path("/secure/getUser", () -> { 
				   
				  get(Controller::getUser); 
				  //logDis.debug("");
				  });
			  
			  path("/secure/userTix", () -> { 
				  logDis.trace("User Tickets"); 
				  get(Controller::getUserTickets); 
				  });
			  

			  
			  
				path("/secure/add", () -> {
					post(Controller::add);
				});
	
				path("/secure/password", () -> {
					logDis.trace("password");
					post(Controller::password);
				});

		});
	}
	
	public static void mgrPaths (Javalin server) {
		server.routes(() -> {
		  path("/mgr/pendingTix", () -> { 
			  logDis.trace("Pending Tix"); 
			  get(Controller::getPending); 
			  });
		  
		  path("/mgr/approve", () -> { 
			  logDis.trace("Approve Tix"); 
			  post(Controller::approveTix); 
			  });
		  
		  path("/mgr/deny", () -> { 
			  logDis.trace("Deny Tix"); 
			  post(Controller::denyTix); 
			  });
		  
		  path("/mgr/add", () -> { 
			  logDis.trace("Create user"); 
			  post(Controller::newUser); 
			  });
		});
	}
}
////////////////////////////////////////////////////
/*
 * path("/EmployeeHome.html", () -> { get(null); // endpoint: /api/heroes
 * method: get path("{index}", () -> { get(null); // endpoint:
 * /api/heroes/{index} method: get put(null); // endpoint: /api/heroes/{index}
 * method: put }); });
 * 
 * path("/admin/herospeak", () -> { get(null); // endpoint: /admin/herospeak
 * method: get }); }); /
 */
