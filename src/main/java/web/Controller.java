/**
 * 
 */
package web;
import java.sql.Blob;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.jetty.util.ajax.JSON;

import Models.Reimb;
import Models.Role;
import Models.Status;
import Models.Type;
import Models.User;
import fastMoney.Service;
import io.javalin.http.Context;

/**
 * @author Kienen
 *
 */
public class Controller {
	final static Logger logCon = Logger.getLogger(Controller.class);
	public static Service service = new Service();
	
	

	/**
	 * @param service
	 */
	public Controller(Service serve) {
		service = serve;
	}

	/**
	 * 
	 */
	public Controller() {
		super();
		service = new Service();
	}

	public static void getallrequests (Context context) {
		//context.json(Service.viewTickets());
		//context.  //attribute("blue", "Fork");
		context.json(context.attributeMap());
	}
	
	public static void login (Context context) {
		logCon.trace("login");
		String username = context.formParam("myUsername");
		String password = context.formParam("myPassword");
		logCon.debug(username);
		logCon.debug(password);
		
		User user = service.login(username, password);
		if (user != null) {
			context.sessionAttribute("currentUser", user);
			Dispatcher.logDis.debug(context.sessionAttributeMap());
			if (user.getRole() == Role.EMPLOYEE) {
				context.redirect("/secure/EmployeeHome.html");
			} else {
				context.redirect("/mgr/ManagerHome.html");
			}
			//context.sessionAttribute("fName", user.g);
		} else {
			
			context.redirect("/");
		}
//		context.sessionAttribute("currentUser", "blue");
//		context.sessionAttribute("mything", 5);
//		
		context.json(context.sessionAttributeMap());
//		context.json(Service.viewTickets());
//		System.out.println(context.sessionAttributeMap());
	}
	
	public static void viewUserTickets (Context ctx) {
		logCon.trace("viewUserTickets");
		if (ctx.sessionAttribute("currentUser") != null) {
			service.viewTicketsByUser(ctx.sessionAttribute("currentUser"));
			logCon.debug(ctx.sessionAttributeMap());
		}
		
	}
	
	public static void add (Context ctx) {
		User user = ctx.sessionAttribute("currentUser");
		
		Reimb newTicket = new Reimb(
				Double.parseDouble(ctx.formParam("amount")), 
				Timestamp.from(Instant.now()), 
				ctx.formParam("description"), 
				user.getId(),
				Type.valueOf(ctx.formParam("Type"))
				) ;
		
		boolean success = service.createTicket(newTicket);
		logCon.debug("created " + success);
		ctx.redirect("/secure/EmployeeHome.html");
		
		
	}	
	
//	public static void formName (Context ctx) {
//		//
//		
//		logCon.debug(ctx.sessionAttributeMap());
//	}
	
	public  static void getUser (Context ctx) {
		//User currentUser = Service.login("santi", "pass"); //ctx.sessionAttribute("currentUser");
		User currentUser = ctx.sessionAttribute("currentUser");
		ctx.json(currentUser);
		System.out.println(ctx.headerMap());
		logCon.debug(ctx.sessionAttributeMap());
	}
	
	public static void getUserTickets(Context ctx) {
		User currentUser = ctx.sessionAttribute("currentUser");
		ctx.json(service.viewTicketsByUser(currentUser));
		logCon.debug(ctx.sessionAttributeMap());
		
	}
	
	public static void getPending(Context ctx) {
		//User currentUser = ctx.sessionAttribute("currentUser");
		Reimb [] pendingTix = service.viewTicketsByStatus(Status.PENDING);
//		for (Reimb ticket : pendingTix) {
//			String name = Service.findEmployeeName(ticket.getAuthor());
//			
//			ticket.setAuthorName(name);
//		}
		ctx.json(pendingTix);
		logCon.debug(ctx.sessionAttributeMap());
		for (Reimb tix : pendingTix) {
			logCon.debug(tix);
		}
			
	}
	
//	public static void updateTix(Context ctx) {
//		Status status;
//		logCon.debug(ctx.formParamMap());
//		logCon.debug(ctx.formParams("approve"));
//		
//		//System.out.println(id);
//		//for (Map.Entry<String,String> key : ctx.queryParamMap())
//		
//		User MGR = ctx.sessionAttribute("currentUser");
//		if (ctx.formParamMap().containsKey("approve")) {
//			status = Status.APPROVED;
//			int id = Integer.parseInt(ctx.formParam("approve"));
//		} else if (ctx.formParamMap().containsKey("deny")) {
//			status = Status.DENIED;
//			int id = Integer.parseInt(ctx.formParam("deny"));
//		}
//	}
	
	public static void approveTix(Context ctx) {
		String body = ctx.body();
		logCon.debug(body);
		Reimb response = ctx.bodyAsClass(Reimb.class);
		int ticket = response.getId();
		
		User mgr = ctx.sessionAttribute("currentUser");
		boolean success = service.updateStatus(ticket, Status.APPROVED, mgr.getId());
		ctx.result("Approved");
		logCon.debug("Ticket " + ticket + " Approve" + success);
		
	}
	
	public static void denyTix(Context ctx) {
		String body = ctx.body();
		logCon.debug(body);
		Reimb response = ctx.bodyAsClass(Reimb.class);
		int ticket = response.getId();
		
		User mgr = ctx.sessionAttribute("currentUser");
		boolean success = service.updateStatus(ticket, Status.DENIED, mgr.getId());
		logCon.debug("Ticket " + ticket + " Deny" + success);
		ctx.result("Denied");
	}
	
	public static void newUser(Context ctx) {
//		String body = ctx.body();
//		logCon.debug(body);
		User newUser = ctx.bodyAsClass(User.class);
		
		
		//User mgr = ctx.sessionAttribute("currentUser");
		boolean success = service.createUser(newUser);
		logCon.debug("User " + newUser  + success);
		if (success) {
			ctx.result("User Created");
		} else {
			ctx.result("Fail");
		}
		
	}	
	
	public static void password(Context ctx) {
		User user = ctx.sessionAttribute("currentUser");
		String oldPass = ctx.formParam("oldPass");
		String newPass = ctx.formParam("newPass");
		boolean success = service.updatePassword(user, oldPass, newPass);
		
		if (success) {
			ctx.result("Password Updated");
		} else {
			ctx.result("Fail");
		}
	}
	
//	public static void denyTix(Context ctx) {
//		int ticket = Integer.parseInt(ctx.formParam("id"));
//		User mgr = ctx.sessionAttribute("currentUser");
//		boolean success = Service.updateStatus(ticket, Status.DENIED, mgr.getId());
//		logCon.debug("Ticket " + ticket + "Deny" + success);
//	}

}
