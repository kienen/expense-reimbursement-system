package example.calc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CalculatorTest {
	
	example.Calc calc = new example.Calc(); 

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void addTestUsingPositiveNumbers() {
		System.out.println("In Add Positive Number Test");
		
		//Assertions.
		assertEquals(15, calc.add(1,14));
		assertEquals(15, calc.add(1,14));
		assertEquals(15, calc.add(1,14));
		
		//Assertions.
	}
	
	@DisplayName("Self Documenting")
	void addTestUsingNegativeNumbers () {
		assertEquals(-15, calc.add(-1,-14));
	}
	
	
//	@Test
//	@Disabled
//	public void myArrayTest() {
//		String myArray = calc.arrayReturn();
//		
//		assertAll(
//				() -> assertEquals("unooooo", myArray[0]),
//				() -> assert
//				);
//	}
	
//	public UserAccount duplicateUser(UserAccount original) {
//		return new UserAccount(original.getUsername(), original.getPassword)
//	}
	

}
