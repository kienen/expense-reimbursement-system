## Expense Reimbursement System
---
# Project Description
The Expense Reimbursement System manages the process of reimbursing employees for expenses incurred while on company time. 
---
# Technologies Used

- Java 
- JavaScript
- Javalin
- JDBC
- PostgreSQL
- JUnit
- Log4j

---
# Features
- Add reimbursements
- View reimbursemnts sorted by author or status
- Email a new user login credentials
- Passwords hashed and salted with 256 bit encryption

