SELECT r.*
, concat( u2.user_first_name, ' ', u2.user_last_name ) AS "resolver_name" 
, concat( u.user_first_name, ' ', u.user_last_name) AS "auth_name"
FROM ers_reimbursement r 
LEFT JOIN ers_users u ON r.reimb_author = u.ers_users_id
LEFT JOIN ers_users u2 ON r.reimb_resolver = u2.ers_users_id

;  